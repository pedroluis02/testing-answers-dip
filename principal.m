function principal(imagen_dir='test_images/eval2.jpg')
  imagen = imread(imagen_dir);	
  imagen_gris = escalaGrises(imagen, 1);
  imagen_b = binarizacionGlobal(imagen_gris, 244);
  
  [fil,col]=size(imagen_gris);
  suma = 0; bandera = 0; nb = 0; aux = 0;
      
  k = 1;
  C = [];
  for j=col: -1 : 1
    p = imagen_b(88, j);
    if(p == 0 && bandera == 0)
        bandera = 1;
        suma = suma + 1;
        aux = j;
    elseif bandera == 1 && p == 0
        suma = suma + 1;
    elseif bandera == 1 && p == 255
        if suma == 1 
            C = [aux C];
            nb = nb + 1;
            if(nb == 3) 
                break;
            end
        end
        
        suma = 0;
        bandera = 0;
        aux = 0;
    end
  end
  
  suma = 0; bandera = 0; nb = 0; aux = 0;
  F = [];
  for i=fil: -1 : 1
    p = imagen_b(i, 271);
    if(p == 0 && bandera == 0)
        bandera = 1;
        suma = suma + 1;
        aux = i;
    elseif bandera == 1 && p == 0
        suma = suma + 1;
    elseif bandera == 1 && p == 255
        if suma == 1 
            F = [aux F]; 
            nb = nb + 1;
            if(nb == 4) 
                break;
            end
        end
        
        suma = 0;
        bandera = 0;
        aux = 0;
    end
  end

  f = length(F);
  c = length(C);
letras=['a' 'b'];
  color = 0;
  cuadrante = [];
  a = [];
  cont=1;
  for i=1 : (f - 1)
    a = [];
    cont=1;
    for j=1 : (c - 1)
     
      cuadrante = [(F(i) + 1) (F(i + 1) - 1) (C(j) + 1) (C(j + 1) - 1)];
      total =  cantidadColorCuadrante(imagen_b, cuadrante, color);
      a = [a total];
      if total>=240
      	disp(letras(cont));
      	end
      cont=cont+1;
    end
    %disp(a);
  end 
  
  %i1 = imread('prueba1.jpg');
  %i1g = escalaGrises(i1, 1);
  %i1b = binarizacionGlobal(i1g, 244);
  %[f1, c1] = size(i1g);
  %cuadrante = [1 f1 1 c1];
  
  %h1 = histogramaGray(i1b, cuadrante);
  
  %i2 = imread('prueba2.jpg');
  %i2g = escalaGrises(i2, 1);
  %i2b = binarizacionGlobal(i2g, 244);
  %[f2, c2] = size(i2g);
  %cuadrante = [1 f2 1 c2];
  %h2 = histogramaGray(i2b, cuadrante);
  
  %disp(h1(1));
  %disp(h2(1));
        
end