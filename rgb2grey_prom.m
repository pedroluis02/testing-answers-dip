function imagengris = rgb2grey_prom( I )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%[filas,columnas]=size(I);
%[fil, col, p]=size(I);
%imagengris = zeros(fil, col, p);
%r = 0; g = 0; b = 0; p = 0;
imagengris = ( (I(:,:,1)/3 + I(:,:,2)/3 + I(:,:,3)/3) );
figure;
subplot(1,2,1);
imshow(I);title('1.IMAGEN RGB ORIGINAL');
subplot(1,2,2);
imshow(imagengris);title('2. IMAGEN GRIS-PROMEDIO');

end