function image_Binarizada = binarizacionGlobal(imagen_gris, umbral)
[fil, col] = size(imagen_gris);

image_Binarizada = uint8(zeros(fil, col));
for m=1 : 1 : fil
    for n=1 : 1 : col
        if (imagen_gris(m, n) >= umbral) %  es el umbral
            image_Binarizada(m, n) = 255;
        else
            image_Binarizada(m, n) = 0;
        end
    end
end

end