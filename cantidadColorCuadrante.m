function cantidad = cantidadColorCuadrante(imagen, cuadrante, color)
   fila_inicio = cuadrante(1); fila_fin = cuadrante(2);
   columna_inicio = cuadrante(3); columna_fin = cuadrante(4);
   
   cantidad = 0;
   
   for i = fila_inicio : fila_fin
        for j = columna_inicio : columna_fin
             if color == imagen(i, j)
                cantidad = cantidad + 1;
             end
        end
    end
end