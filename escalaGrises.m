% Funcion: recibe una imagen en RGB y retorna otra en escala de grises
% imagenRGB: matriz de imagen en RGB
% tipo: tipo de escalado(1 | 2)
	% 1: escalado por promedio
	% 2: escalado por porcentaje %
function imagenGray = escalaGrises(imagenRGB, tipo)
   if tipo == 1
      imagenGray = uint8( imagenRGB(:, :, 1)*(0.3) + imagenRGB(:, :, 2)*(0.59) + imagenRGB(:, :, 3)*(0.11) );
   elseif tipo == 2
      imagenGray = uint8( imagenRGB(:, :, 1)/3 + imagenRGB(:, :, 2)/3 + imagenRGB(:, :, 3)/3 );
   else
      imagenGray = [];
   end
end